# 第六讲 物体检测在机械臂中的应用

## 1. 物体检测简述

物体检测（Object detection）除了需要对物体进行分类外，还需要检测出物体的具体位置坐标。将物体检测应用到机器人或机械臂上，能够帮助机器人感知周围环境，识别特定物体，实现“手眼协调”抓取、物品分拣、定位、人脸识别、故障检测等多种功能，大大扩展机器人的“智能化”以及应用范围。

目前最常用的物体检测方法有两大类：基于特征匹配的物体检测和基于深度学习的物体检测。

基于特征匹配的算法通常会提取图像中的特征点，通过对比图像之间的特征点进行物体检测。特征点是图像中“有代表性”的点，由关键点和描述 两部分组成，FAST (Features From Accelerated Segment Test)、SIFT( ScaleInvariant Feature Transform)、SURF(Speeded Up Robust Feature)、ORB(Oriented Fast and Rotated Brief)是几种常见的特征提取算法，在OpenCV等图像处理库中都有实现接口。

基于深度学习的物体检测兴起于2013年以后，能够自动从数据中学习特征。从R-CNN、OverFeat，到后面的Fast/Faster R-CNN、SSD、YOLO系列，再到2018年的Pelee算法，基于深度学习的物体检测大大提高了物体检测性能。

ROS中提供了几种常用的物体检测算法的封装包，本章节将介绍find_object_2d、object_recognition_core(ORK)和darknet_ros(YOLO)三个物体检测ROS开源功能包的安装和使用。物体检测的具体算法原理不在本课程教学范围内，感兴趣的同学可自行学习。

## 2. find_object_2d的安装和测试

### 2.1 find_object_2d功能包简介

find_object_2d功能包提供了Find-Object[6]应用的ROS封装，能够使用简单的Qt界面选择SIFT、SUFT、FAST等其他特征检测算法，用于物体检测测试。该功能包的输入为ROS图像消息，将包含物体ID 和在图像中的位置的信息发布到objects和objectsStamped话题上。

find_object_2d功能包里提供了两个节点：find_object_2d及find_object_3d。其中find_object_3d是为Realsense之类的深度相机准备的，可以通过在匹配目标后识别目标中心的深度信息输出目标的三维坐标。

可直接使用apt安装的方式安装find-object-2d功能包：

```
$ sudo apt-get install ros-kinetic-find-object-2d
```

以上是ROS-kinetic版本下的安装，若使用其他ROS版本，只需讲kinetic替换为对应的ROS版本即可。

## 2.2 find_object_2d节点的测试

- 启动相机驱动节点，这里以RealsenseD415深度相机为例进行测试

```
$ roslaunch realsense2_camera rs_rgbd.launch
```

- 相机的彩色图像话题名为/camera/color/image_raw，所以在启动find_object_2d节点时，节点的参数image需设为/camera/color/image_raw。新开终端，输入以下命令启动2D检测节点：

```
$ rosrun find_object_2d find_object_2d image:=/camera/color/image_raw 
```

- 启动后，会弹出用于物体检测的GUI窗口Find-Object如图1所示:

![IMG_256](src/images/fig1.png)

图 1 物体检测

- 鼠标右键点击上图Find-Object窗口左侧Objects下的空白页面，会弹出两个选择：Add Object From Scene （从图像添加物体）和Add Object From files（从文件添加物体）。这里我们选择Add Object From Scene 从当前画面中添加需要被检测的物体：

![IMG_256](src/images/fig2.png)

图 2 从当前画面添加物体

- 在上图点击Take picture 按钮截图拍照

![IMG_256](src/images/fig3.png)

图 3 截图拍照

- 在截好的画面上用鼠标左键选择想要识别的物体的区域如下图所示。黄色点表示的是特征点，在使用find_object_2d进行物体检测时，尽量选择特征点较多的物品用于识别。

![IMG_256](src/images/fig4.png)

图 4 识别物体区域

\- 选择好区域后，点击Next按钮得到下图：

![IMG_256](src/images/fig5.png)

图 5 点击Next

- 若此时的选择符合自己的需求，可点击End按钮结束目标物体的选择。选择的物体会添加到Find-Object窗口左侧的Objects下，此时右侧摄像头拍摄的实时画面中，可以看到四边形框框出的的检测到的物体：

![IMG_256](src/images/fig6.png)

图 6 检测出目标物体

- 可按照上面的方式继续在窗口左侧的Objects下右键添加其他物体。

- 新开一个终端，可以使用rostopic echo 命令查看节点find_object_2d节点发布的/objects话题信息：

```
$ rostopic echo /objects
```

![IMG_256](src/images/fig7.png)

图 7 查看objects话题信息

- /objects话题的消息类型为std_msgs/Float32MultiArray[8]，data数据的含义：[物体1的ID，物体1的横向宽度，物体1的纵向长度，h11， h12，h13，h21，h22， h23，h31，h32，h33，物体2的ID... ]。hxx是3*3H矩阵。可参考链接[9]里的代码对H矩阵进行处理，得到物体边框的四个角的像素坐标。

- objectsStamped话题发布的是带Head消息头的物体检测信息。

- 在运行find_object_2d节点的终端按下Ctrl+C关闭程序时，会弹出如下提示框提示是否想要保存刚才添加的目标物体的图片：

![IMG_256](src/images/fig8.png)

图 8 保存目标物体提示

- 若选择Yes，会弹出保存路径选择窗口，选择想要保存的路径，则可以.png图片的形式将目标物体保存下来。下图所示是保存在xarm_vision/config目录里的目标物图片：

![2](src/images/fig9.png)

图 9 保存的目标物图片

- 再启动find_object_2d节点进行物体检测时，就可在Objects下鼠标右键选择Add Object From files从图片保存的路径中选择图片添加，同样可以用于物体检测，测试结果如下：

![IMG_256](src/images/fig10.png)

图 10 目标物体检测测试

## 2.3 find_object_3d节点的测试

find_object_2d节点最终可以得到框出目标物体的四边形的四个顶点的像素坐标以及四边形中心点的坐标，若使用深度相机，根据物体中心在像素平面内的坐标，可得到此处的深度信息，从而转化成目标物体中心到相机坐标系的坐标转换关系，这就是find_object_3d的简单原理。下面对find_object_3d进行测试。

- 启动相机驱动节点，这里以RealsenseD415深度相机为例进行测试：

```
$ roslaunch realsense2_camera rs_rgbd.launch
```

- rostopic list命令可以查看相机发布的话题有哪些，其中我们需要的是彩色图像话题/camera/color/image_raw、深度图像话题/camera/aligned_depth_to_color/image_raw以及内参标定参数话题/camera/aligned_depth_to_color/camera_info。

- 可以参考find_object_2d/launch里的find_object_3d.launch文件编写我们自己的3d检测launch文件。本课程已经编写好并放在了xarm_vision/launch中，内容如下：

![1](src/images/fig11.png)

图 11 3d检测launch文件

- 若使用其他相机，第12～13行的话题名需进行修改。第17～18行我们设置了base_link到camera_link的TF变换，这里的参数只是虚拟值，与真实机械臂到camera_link的坐标变换不一致。下一章我们将学习如何得到base_link到camera_link的TF变换。

- 输入以下命令启动3D检测节点：

```
$ roslaunch xarm_vision find_object_3d.launch 
```

- 启动后，会弹出用于物体检测的GUI窗口Find-Object，添加检测物体进行检测的操作与2D检测时一致，这里不再赘述：

![IMG_256](src/images/fig12.png)

图 12 添加物体检测

- 新开一个终端输入以下命令启动RViz，并点击Add按钮添加TF插件，可看到目标物体到camera_link再到base_link的坐标转换关系：

```
$ rosrun rviz rviz
```

![IMG_256](src/images/fig13.png)

图 13 目标物体到base_link的坐标转换关系

\- 若camera_link到base_link的坐标变换符合实际变换关系，我们也就相当于得到了目标物体在base_link坐标系下的位姿，此时若让机械臂进行抓取，理论上就能抓取成功。这也是手眼协调抓取的基本原理，这一部分将在第六章进行详细介绍。

\- 注意：我们这里得到的目标物体中心的距离，并不是物体实际的中心，而是物体面对摄像头一侧的表面的中心，真正的中心还应加上物体的厚度（深度），所以这种方法得到的位姿并不准确。

## 3. ORK的安装和测试

### 3.1 ORK项目简介

Object Recognition Kitchen (ORK)是一个目标检测项目，目标可以是带纹理的、不带纹理的、透明的等等，ORK解决了数据库管理、输入输出处理、ROS集成等非视觉方面的问题，集成了LINE-MOD、tabletop、TOP等物体识别方法[10]。

LINE-MOD主要用于2D或3D刚性物体的检测与定位，能在较短的时间内训练采集到的物体信息，通过模板匹配对比得到目标物体的ID、位姿和置信度信息，算法原理可参考论文[11]。本节课将介绍ORK的安装过程以及训练可乐罐数据进行目标检测。

具体安装和测试过程也可参考官网 [12]：

http://wg-perception.github.io/ork_tutorials/index.html

### 3.2 ORK的安装部署

- 安装测试环境：Ubuntu16.04 + ROS-kinetic

- 安装依赖库：

```
$sudo apt-get install meshlab

$sudo apt-get install libosmesa6-dev

$sudo apt-get install python-pyside.qtcore

$sudo apt-get install python-pyside.qtgui
```

- 采用源码下载安装的方式。先进入到ROS工作空间的src目录下，新建一个ork目录，用于存放下载的ork功能包：

```
$cd ~/catkin_ws/src

$mkdir ork
```

- 进入刚才进行的ork文件夹，通过git clone 命令下载源码：

```
$cd ork

$git clone http://github.com/wg-perception/linemod

$git clone http://github.com/wg-perception/object_recognition_core

$git clone http://github.com/wg-perception/object_recognition_msgs

$git clone http://github.com/wg-perception/object_recognition_ros

$git clone http://github.com/wg-perception/object_recognition_ros_visualization

$git clone http://github.com/wg-perception/ork_renderer

$git clone https://github.com/wg-perception/ork_tutorials.git

$git clone http://github.com/wg-perception/tabletop

$git clone http://github.com/wg-perception/tod

$git clone http://github.com/wg-perception/transparent_objects

$git clone http://github.com/wg-perception/capture

$git clone http://github.com/wg-perception/reconstruction
```

- 编译代码：

```
$cd ~/catkin_ws

$rosdep install --from-paths src -i -y

$catkin_make 
```

- 若编译通过，说明安装成功。

### 3.3 CouchDB建立模型库

在使用ORK进行物体识别前，首先需要准备好目标物体的模型文件，必须为.stl或者obj格式。可以创建自己的模型，也可使用网上提供的免费模型文件。有了模型后，需要确保模型的尺寸大小正确且需要注意一下模型的原点，ORK最后输出的物体的位姿就是模型的原点的位置。

![IMG_256](src/images/fig14.png)

图 14 可乐罐和水瓶模型

如图14所示的可乐罐和水瓶模型，两者的原点不一样，可乐罐的原点在中心，所以后面进行识别时，输出的位姿也是指的可乐罐的中心的位姿。

有了模型文件后，可按照下面的步骤安装CouchDB数据库系统并建立目标物体模型文件的数据库信息：

- 安装couchdb：

```
$sudo apt-get install couchdb
```

- 测试是否安装成功：

```
$curl -X GET http://localhost:5984
```

- 如图15所示，有版本等信息则说明安装成功

![IMG_256](src/images/fig15.png)

图 15 安装成功

- 在数据库中创建一个可乐模型的数据：

```
$ rosrun object_recognition_core object_add.py -n "coke " -d "A universal can of coke" --commit
```

- 如图16所示成功后，生成的id需要保存，后面添加模型文件以及模型训练时都需要用到：

![ork2 (1)](src/images/fig16.png)

图 16 生成ID

- 可以在浏览器中查看创建的可乐数据：

http://localhost:5984/_utils/database.html?object_recognition/_design/objects/_view/by_object_name

![IMG_258](src/images/fig17.png)

图 17 查看可乐数据

- 在 ork_tutorials /data/中包含了一个可乐罐模型 coke.stl， 运行下面命令将stl模型文件添加至数据库中，注意id 和stl文件的路径要进行修改。

```
$ rosrun object_recognition_core mesh_add.py [id] [路径] --commit
```

![IMG_256](src/images/fig18.png)

图 18 将stl模型文件添加至数据库

- 安装 couchapp 工具，在浏览器中查看具体的模型：

```
$ sudo pip install git+https://github.com/couchapp/couchapp.gitrosrun object_recognition_core push.sh
```

- 一般需要翻墙才能打开，看不到的话也没有关系，不影响后面的训练和识别：

![IMG_260](src/images/fig19.png)

![IMG_261](src/images/fig20.png)

图 19 查看据图模型

- 可以用上面的步骤继续添加其他模型的数据库。

- 若要删除某个目标物体，可运行以下命令删除：

```
 $ rosrun object_recognition_core object_delete.py [id] --commit
```

### 3.4 模型训练以及LINE-MOD物体识别

- 运行以下命令进行模型训练，需要几分钟时间：

```
$rosrun object_recognition_core training -c `rospack find object_recognition_linemod`/conf/training.ork
```

- 训练完如图20所示；

![IMG_256](src/images/fig21.png)

图 20 模型训练

- 以RealsenseD415深度相机为例，对LINE-MOD物体识别进行测试。

- 启动相机：

```
$roslaunch realsense2_camera rs_rgbd.launch
```

- 打开linemod/conf/目录里的detection.ros.ork，修改source1如下，让定阅的图像话题与RealsenseD415对应：

\1.    source1:

\2.     type: RosKinect

\3.     module: 'object_recognition_ros.io'

\4.     parameters:

\5.      rgb_frame_id: 'camera_color_optical_frame'

\6.      rgb_image_topic: '/camera/color/image_raw'

\7.      rgb_camera_info: '/camera/color/camera_info'

\8.      depth_frame_id: 'camera_depth_optical_frame'

\9.      depth_image_topic: '/camera/aligned_depth_to_color/image_raw'

\10.     depth_camera_info: '/camera/aligned_depth_to_color/camera_info'

\- 运行 detection 节点进行物体检测：

```
$rosrun object_recognition_core detection -c `rospack find object_recognition_linemod`/conf/detection.ros.ork
```

- 在终端运行以下命令启动rviz：

```
$rosrun rviz 
```

- 在RViz中点击Add添加Pointcould2插件，选择话题/camera/depth_registered/points；再点击Add按钮添加OrkObject,，选择话题/recognized_object_array；可在显示界面看到识别到的可乐罐，坐标的中心位于可乐罐的中心。

![IMG_256](src/images/fig22.png)

图 21 识别可乐罐

- 可通过话题查看识别的结果：

```
$rostopic echo /recognized_object_array
```

- /recognized_object_array的消息类型为object_recognition_msgs/RecognizedObjectArray .msg，可以使用rosmsg show 命令查看消息的具体信息：

```
$ rosmsg show object_recognition_msgs/RecognizedObjectArray 
```

- 有多大的可能性是这个物体；3）pose位姿信息，表示目标物体相对于相机坐标系的位置和姿态。

- 有了目标物体到相机的坐标变换后，只要知道相机到机械臂底座base_link的坐标变换后，就能进行目标抓取、目标跟踪等一系列其他操作了。

## 4. darknet_ros的安装和测试

darknet_ros是实时物体检测YOLO系列的ROS封装包，这是一种基于深度学习的物体检测方法，有关YOLO算法的详细说明可参考链接[14]。darknet_ros的代码仓库链接[15]：

https://github.com/leggedrobotics/darknet_ros

采用源码安装的方式进行安装：

- 进入ROS工作空间src目录：

```
$ cd ~/catkin_ws/src
```

- 下载源码：

```
$ git clone --recursive https://github.com/leggedrobotics/darknet_ros.git
```

- 编译：

```
$ cd ..

$ catkin_make -DCMAKE_BUILD_TYPE=Release
```

- 因为要下weights权重文件，所以编译时时间较长。编译通过后，说明安装成功。测试前，需要先确认权重文件是否下载成功并修改启动配置文件：

- 进入darknet_ros/yolo_network_config/weights/目录，参考how_to_download_weights.txt的下载说明，选择想要测试的算法下载对应的权重文件：

![1](src/images/fig23.png)

图 22 how_to_download_weights.txt

- 这里我们以yolov3为例进行说明。在终端输入以下命令进行下载：

```
$ roscd darknet_ros/yolo_network_config/weights/
$ wget http://pjreddie.com/media/files/yolov3.weights
```

- 下载完成后，修改darknet_ros/darknet_ros/launch/darknet_ros.launch文件里的network_param_file参数为config里的yolov3.yaml：

![11](src/images/fig24.png)

图 23 修改为yolov3.yaml

- 修改darknet_ros/darknet_ros/config/ros.yaml 文件里的订阅的图像话题名与实际的图像名对应，这里用USB摄像头进行测试，所以修改为/usb_cam/image_raw：

![1](src/images/fig25.png)

图 24 修改订阅的图像话题名

- 下面进行测试，先启动相机：

```
$ roslaunch xarm_vision usb_cam.launch
```

- 新开终端，启动检测程序：

```
$ roslaunch darknet_ros darknet_ros.launch
```

- 程序启动几秒后会弹出YOLO V3窗口，可以看到用边框框出来的检测结果类似图25：

![img](src/images/fig26.png)

图 25 检测结果

- darknet_ros.launch启动的终端也会输出如下识别信息，包括帧率FPS以及识别出的物体和置信度：

![22](src/images/fig27.png)

图 26 识别信息

可以看到这里的FPS特别低，画面卡顿严重。熟悉深度学习的同学可以尝试在使用GPU、CUDA、CUDNN等加速，本课程里不做要求。或者可以选择以-tiny为名字后缀的算法进行测试，能提高一定的帧率。

darknet_ros还对识别结果进行了封装，通过ROS话题发布出来：

- /darknet_ros/found_object(darknet_ros_msgs/ObjectCount.msg)：检测到的目标的个数：

![1]src/images/fig28.png)

图 27 检测到目标个数

- /darknet_ros/bounding_boxes(darknet_ros_msgs/BoundingBoxes.msg)：发布边界框数组，以像素坐标的形式提供边界框的位置和大小的信息。

![2](src/images/fig29.png)

图 28 边界框的位置和大小的信息

- /darknet_ros/detection_image(sensor_msgs/Image)：带识别结果的图像信息：

![img](src/images/fig30.png)

图 29 识别结果的图像信息

除了使用提供的基于COCO数据集训练的模型进行测试，还可训练自己的数据集，用以特定目标的识别。

